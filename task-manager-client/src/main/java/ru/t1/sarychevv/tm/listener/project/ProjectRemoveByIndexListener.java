package ru.t1.sarychevv.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    @EventListener(condition = "@ProjectRemoveByIndexListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().removeProjectByIndex(request);
    }

}
