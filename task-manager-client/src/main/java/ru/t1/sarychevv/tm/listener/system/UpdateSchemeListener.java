package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.system.UpdateSchemeRequest;
import ru.t1.sarychevv.tm.dto.response.system.UpdateSchemeResponse;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.listener.AbstractListener;

@Component
public class UpdateSchemeListener extends AbstractListener {
    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update scheme.";
    }

    @Override
    @NotNull
    public String getName() {
        return "update";
    }

    @Override
    @EventListener(condition = "@UpdateSchemeListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE SCHEME]");
        @NotNull final UpdateSchemeResponse response = getSystemEndpoint().updateScheme(new UpdateSchemeRequest(getToken()));
        System.out.println(response);

    }
}
