package ru.t1.sarychevv.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.listener.AbstractListener;

@Component
public class ApplicationHelpListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@ApplicationHelpListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener.getArgument() != null && !listener.getArgument().isEmpty())
                System.out.println(listener.getName() + " - " + listener.getArgument() + " - " + listener.getDescription());
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show commands list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

}
