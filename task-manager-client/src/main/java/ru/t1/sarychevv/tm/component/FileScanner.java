package ru.t1.sarychevv.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner extends Thread {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (@Nullable final AbstractListener listener : listeners) {
            this.commands.add(listener.getName());
        }
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void close() {
        executorService.shutdown();
    }

    private void process() {
        for (@NotNull File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.getListenerByCommand(fileName);
                } catch (@NotNull Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
