package ru.t1.sarychevv.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Update user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    @EventListener(condition = "@UserUpdateProfileListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateUserProfile(request);
    }

}
