package ru.t1.sarychevv.tm.listener.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.field.ProjectEmptyException;
import ru.t1.sarychevv.tm.listener.AbstractListener;

import java.util.List;

@Getter
@Setter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    public ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 0;
        for (final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getId() + ": " + task.getName() +
                    " " + task.getDescription() + " " + Status.toName(task.getStatus()));
            index++;
        }
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new ProjectEmptyException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
