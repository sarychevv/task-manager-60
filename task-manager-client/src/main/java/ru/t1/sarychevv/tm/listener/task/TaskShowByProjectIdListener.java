package ru.t1.sarychevv.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.sarychevv.tm.enumerated.TaskSort;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class TaskShowByProjectIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by project id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    @EventListener(condition = "@TaskShowByProjectIdListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        request.setSort(sort);
        @Nullable final List<TaskDTO> tasks = getTaskEndpoint().getTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}
