package ru.t1.sarychevv.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    private static final Class<TaskDTO> TYPE = TaskDTO.class;

    public TaskDTORepository() {
        super(TYPE);
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId,
                          @NotNull final String name) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId,
                                            @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getTableName())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

}

