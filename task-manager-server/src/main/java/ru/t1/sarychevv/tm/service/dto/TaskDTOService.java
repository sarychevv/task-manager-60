package ru.t1.sarychevv.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    @NotNull
    @Autowired
    private ITaskDTORepository repository;

    @NotNull
    protected ITaskDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeStatusById(@Nullable final String userId,
                                    @Nullable final String id,
                                    @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.update(userId, task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return repository.add(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return repository.add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId,
                                            @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.update(userId, task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.update(userId, task);
        return task;
    }

    @Override
    @Transactional
    public void updateProjectIdById(@Nullable final String userId,
                                    @Nullable final String id,
                                    @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.update(userId, task);
    }

}

