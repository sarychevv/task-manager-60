package ru.t1.sarychevv.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.IRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.model.IService;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.IdEmptyException;
import ru.t1.sarychevv.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    @Autowired
    protected ApplicationContext context;

    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract IRepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) throws Exception {
        return getRepository().add(model);
    }

    @Override
    @Transactional
    public void removeAll() throws Exception {
        getRepository().removeAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return getRepository().findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        return getRepository().findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(id);
    }

    @Override
    public int getSize() throws Exception {
        return getRepository().getSize();
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable final M model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        return getRepository().removeOne(model);
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        return removeOne(result);
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        return getRepository().update(model);
    }

}

