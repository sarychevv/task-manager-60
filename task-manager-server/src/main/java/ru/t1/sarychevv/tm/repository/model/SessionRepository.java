package ru.t1.sarychevv.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.api.repository.model.ISessionRepository;
import ru.t1.sarychevv.tm.model.Session;

@Repository
@Scope("prototype")
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    private static final Class<Session> TYPE = Session.class;

    public SessionRepository() {
        super(TYPE);
    }

}

