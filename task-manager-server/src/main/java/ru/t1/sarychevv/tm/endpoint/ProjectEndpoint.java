package ru.t1.sarychevv.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sarychevv.tm.api.service.dto.IProjectDTOService;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.request.project.*;
import ru.t1.sarychevv.tm.dto.response.project.*;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                                   @NotNull ProjectChangeStatusByIdRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable ProjectDTO project = projectDTOService.changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                                         @NotNull ProjectChangeStatusByIndexRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable ProjectDTO project = projectDTOService.changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectClearResponse clearProject(@WebParam(name = REQUEST, partName = REQUEST)
                                             @NotNull ProjectClearRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        projectDTOService.removeAll(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @WebMethod
    public ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST)
                                               @NotNull ProjectCreateRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable ProjectDTO project = projectDTOService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectGetByIdResponse getProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull ProjectGetByIdRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable ProjectDTO project = projectDTOService.findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectGetByIndexResponse getProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull ProjectGetByIndexRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable ProjectDTO project = projectDTOService.findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectListResponse listProject(@WebParam(name = REQUEST, partName = REQUEST)
                                           @NotNull ProjectListRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final ProjectSort sort = request.getSort();
        @NotNull List<ProjectDTO> projects = projectDTOService.findAll(userId, sort.getComparator());
        return new ProjectListResponse(projects);
    }

    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull ProjectRemoveByIdRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable ProjectDTO project = projectDTOService.removeOneById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                             @NotNull ProjectRemoveByIndexRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable ProjectDTO project = projectDTOService.removeOneByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull ProjectUpdateByIdRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable ProjectDTO project = projectDTOService.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                             @NotNull ProjectUpdateByIndexRequest request) throws Exception {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable ProjectDTO project = projectDTOService.updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
