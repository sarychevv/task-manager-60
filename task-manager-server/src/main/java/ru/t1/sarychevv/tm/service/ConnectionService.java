package ru.t1.sarychevv.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;
import java.sql.DriverManager;

@Service
@NoArgsConstructor
public final class ConnectionService implements IConnectionService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Getter
    @NotNull
    @Autowired
    private EntityManager entityManager;

    @Override
    public void close() {
        entityManagerFactory.close();
    }

    @Override
    @NotNull
    @SneakyThrows
    public Liquibase getLiquibase() {
        @NotNull final Connection connection = DriverManager.getConnection(
                propertyService.getDatabaseUrl(),
                propertyService.getDatabaseUsername(),
                propertyService.getDatabasePassword());
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        @NotNull final String fileName = "changelog/changelog-master.xml";
        @NotNull final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        return new Liquibase(fileName, accessor, database);
    }

}

