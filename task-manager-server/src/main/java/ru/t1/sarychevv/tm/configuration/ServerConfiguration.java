package ru.t1.sarychevv.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.sarychevv.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.sarychevv.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDataBaseDriver());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        dataSource.setUsername(propertyService.getDatabaseUsername());
        dataSource.setPassword(propertyService.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.sarychevv.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDataBaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDataBaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDataBaseShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getDataBaseFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDataBaseSecondLvlCache());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDataBaseFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDataBaseUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDataBaseUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDataBaseRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDataBaseConfigFilePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }
}
