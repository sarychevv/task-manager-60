package ru.t1.sarychevv.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.api.repository.model.IProjectRepository;
import ru.t1.sarychevv.tm.model.Project;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    private static final Class<Project> TYPE = Project.class;

    public ProjectRepository() {
        super(TYPE);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description) throws Exception {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name) throws Exception {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}

