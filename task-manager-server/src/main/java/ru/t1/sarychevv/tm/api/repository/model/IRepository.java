package ru.t1.sarychevv.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    M add(@NotNull M model) throws Exception;

    void removeAll() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    int getSize() throws Exception;

    @NotNull
    M removeOne(@NotNull M model) throws Exception;

    @NotNull
    M update(@NotNull M model) throws Exception;

}

