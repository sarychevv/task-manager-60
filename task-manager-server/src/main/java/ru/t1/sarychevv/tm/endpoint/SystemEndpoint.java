package ru.t1.sarychevv.tm.endpoint;

import liquibase.Liquibase;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.sarychevv.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.sarychevv.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.sarychevv.tm.dto.request.system.DropSchemeRequest;
import ru.t1.sarychevv.tm.dto.request.system.UpdateSchemeRequest;
import ru.t1.sarychevv.tm.dto.response.system.DropSchemeResponse;
import ru.t1.sarychevv.tm.dto.response.system.ServerAboutResponse;
import ru.t1.sarychevv.tm.dto.response.system.ServerVersionResponse;
import ru.t1.sarychevv.tm.dto.response.system.UpdateSchemeResponse;
import ru.t1.sarychevv.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private IConnectionService connectionService;

    @Override
    @NotNull
    @WebMethod
    public ServerAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST)
                                        @NotNull final ApplicationAboutRequest request) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NotNull
    @WebMethod
    public ServerVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST)
                                            @NotNull final ApplicationVersionRequest request) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @Override
    @WebMethod
    @SneakyThrows
    @NotNull
    public DropSchemeResponse dropScheme(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final DropSchemeRequest request) {
        check(request, Role.ADMIN);
        @NotNull final Liquibase liquibase = serviceLocator.getConnectionService().getLiquibase();
        liquibase.dropAll();
        return new DropSchemeResponse(null);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @NotNull
    public UpdateSchemeResponse updateScheme(@WebParam(name = REQUEST, partName = REQUEST)
                                             @NotNull final UpdateSchemeRequest request) {
        check(request, Role.ADMIN);
        @NotNull final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
        return new UpdateSchemeResponse(null);
    }

}
