package ru.t1.sarychevv.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    private static final Class<ProjectDTO> TYPE = ProjectDTO.class;

    public ProjectDTORepository() {
        super(TYPE);
    }

    @Override
    public @NotNull ProjectDTO create(@NotNull String userId,
                                      @NotNull String name,
                                      @NotNull String description) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @Override
    public @NotNull ProjectDTO create(@NotNull String userId,
                                      @NotNull String name) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }
}
