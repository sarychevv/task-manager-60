package ru.t1.sarychevv.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.sarychevv.tm.api.service.model.IUserOwnedService;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.IdEmptyException;
import ru.t1.sarychevv.tm.exception.field.IndexIncorrectException;
import ru.t1.sarychevv.tm.exception.field.StatusEmptyException;
import ru.t1.sarychevv.tm.exception.field.UserIdEmptyException;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().add(userId, model);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().removeAll(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId,
                              @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(userId, id);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId,
                           @Nullable final Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId,
                           @Nullable final Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return getRepository().findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId,
                         @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId,
                            @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return getRepository().findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().getSize(userId);
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable final String userId,
                       @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return getRepository().removeOne(userId, model);
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String userId,
                           @Nullable final String id) throws Exception {
        @Nullable M result = findOneById(userId, id);
        return removeOne(userId, result);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId,
                              @Nullable final Integer index) throws Exception {
        @Nullable M result = findOneByIndex(userId, index);
        removeOne(userId, result);
        return result;
    }

    @Override
    @Transactional
    public void update(@Nullable final String userId,
                       @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        getRepository().update(userId, model);
    }

    @NotNull
    @Override
    @Transactional
    public M changeStatusById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        if (status == null) throw new StatusEmptyException();
        getRepository().update(userId, model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M changeStatusByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final Status status) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(userId, index);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (status == null) throw new StatusEmptyException();
        getRepository().update(userId, model);
        return model;
    }

}

