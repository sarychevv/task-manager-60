package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    @NotNull
    TaskDTO create(@Nullable String userId,
                   @Nullable String name) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId,
                                     @Nullable String projectId) throws Exception;

    void updateProjectIdById(@Nullable final String userId,
                             @Nullable final String id,
                             @Nullable final String projectId) throws Exception;

}

