package ru.t1.sarychevv.tm.dto.request.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

public final class UpdateSchemeRequest extends AbstractUserRequest {

    public UpdateSchemeRequest(@Nullable final String token) {
        super(token);
    }

}
